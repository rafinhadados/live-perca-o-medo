```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
```


```python
!ls *.csv
```

    2feats.csv	       calendar.csv  listings_details.csv  reviews.csv
    available_more_20.csv  listings.csv  neighbourhoods.csv    reviews_details.csv



```python
df = pd.read_csv('available_more_20.csv')
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>listing_id</th>
      <th>price</th>
      <th>available_more_20</th>
      <th>latitude</th>
      <th>longitude</th>
      <th>minimum_nights</th>
      <th>a'dam</th>
      <th>amazing</th>
      <th>amstel</th>
      <th>amsterdam!</th>
      <th>...</th>
      <th>unique</th>
      <th>very</th>
      <th>vibrant</th>
      <th>view</th>
      <th>vondelpark</th>
      <th>west</th>
      <th>westerpark</th>
      <th>room_type_Entire home/apt</th>
      <th>room_type_Private room</th>
      <th>room_type_Shared room</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2818</td>
      <td>63.051613</td>
      <td>1</td>
      <td>52.365755</td>
      <td>4.941419</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3209</td>
      <td>161.326165</td>
      <td>0</td>
      <td>52.390225</td>
      <td>4.873924</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>20168</td>
      <td>112.847201</td>
      <td>1</td>
      <td>52.365087</td>
      <td>4.893541</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>25428</td>
      <td>125.000000</td>
      <td>0</td>
      <td>52.373114</td>
      <td>4.883668</td>
      <td>14</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>27886</td>
      <td>148.477547</td>
      <td>0</td>
      <td>52.386727</td>
      <td>4.892078</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>10671</th>
      <td>30576148</td>
      <td>349.000000</td>
      <td>0</td>
      <td>52.345999</td>
      <td>4.952145</td>
      <td>7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10672</th>
      <td>30577727</td>
      <td>189.000000</td>
      <td>0</td>
      <td>52.362412</td>
      <td>4.932467</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10673</th>
      <td>30578037</td>
      <td>80.000000</td>
      <td>1</td>
      <td>52.362431</td>
      <td>4.926912</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10674</th>
      <td>30579673</td>
      <td>65.000000</td>
      <td>0</td>
      <td>52.363780</td>
      <td>4.932493</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10675</th>
      <td>30580413</td>
      <td>220.000000</td>
      <td>0</td>
      <td>52.346911</td>
      <td>4.901932</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>10676 rows × 106 columns</p>
</div>




```python
t0 = df.available_more_20==0
t1 = df.available_more_20==1

df.loc[t1]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>listing_id</th>
      <th>price</th>
      <th>available_more_20</th>
      <th>latitude</th>
      <th>longitude</th>
      <th>minimum_nights</th>
      <th>a'dam</th>
      <th>amazing</th>
      <th>amstel</th>
      <th>amsterdam!</th>
      <th>...</th>
      <th>unique</th>
      <th>very</th>
      <th>vibrant</th>
      <th>view</th>
      <th>vondelpark</th>
      <th>west</th>
      <th>westerpark</th>
      <th>room_type_Entire home/apt</th>
      <th>room_type_Private room</th>
      <th>room_type_Shared room</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2818</td>
      <td>63.051613</td>
      <td>1</td>
      <td>52.365755</td>
      <td>4.941419</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>20168</td>
      <td>112.847201</td>
      <td>1</td>
      <td>52.365087</td>
      <td>4.893541</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>28658</td>
      <td>65.215456</td>
      <td>1</td>
      <td>52.375342</td>
      <td>4.857289</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>31080</td>
      <td>219.000000</td>
      <td>1</td>
      <td>52.351321</td>
      <td>4.848383</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>42970</td>
      <td>139.260737</td>
      <td>1</td>
      <td>52.367814</td>
      <td>4.890012</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>10661</th>
      <td>30554026</td>
      <td>142.646524</td>
      <td>1</td>
      <td>52.359957</td>
      <td>4.926044</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10663</th>
      <td>30556632</td>
      <td>163.400000</td>
      <td>1</td>
      <td>52.371920</td>
      <td>4.900377</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10665</th>
      <td>30556993</td>
      <td>350.000000</td>
      <td>1</td>
      <td>52.370966</td>
      <td>4.882705</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10668</th>
      <td>30562689</td>
      <td>83.688602</td>
      <td>1</td>
      <td>52.356327</td>
      <td>4.933991</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10673</th>
      <td>30578037</td>
      <td>80.000000</td>
      <td>1</td>
      <td>52.362431</td>
      <td>4.926912</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>4158 rows × 106 columns</p>
</div>




```python
plt.figure()
plt.hist(df.loc[t0].price,rwidth=.90,color='b',alpha=.3, bins=np.linspace(0,8000,50))
plt.hist(df.loc[t1].price,rwidth=.90,color='r',alpha=.3, bins=np.linspace(0,8000,50));
```


    
![png](live_20210113_files/live_20210113_4_0.png)
    



```python
t0.sum()
```




    6518




```python
t1.sum()
```




    4158




```python
plt.figure(figsize=(15,5))
plt.hist(df.loc[t0].price,rwidth=.90,color='b',alpha=.3, bins=np.linspace(0,8000,50), density=True)
plt.hist(df.loc[t1].price,rwidth=.90,color='r',alpha=.3, bins=np.linspace(0,8000,50), density=True);
```


    
![png](live_20210113_files/live_20210113_7_0.png)
    



```python
plt.figure(figsize=(15,5))
plt.boxplot([df.loc[t0].price, df.loc[t1].price],positions=[1,1.2]);
plt.ylim(0,500)
plt.grid()
```


    
![png](live_20210113_files/live_20210113_8_0.png)
    



```python
plt.figure(figsize=(15,5))
plt.boxplot([df.loc[t0].minimum_nights, df.loc[t1].minimum_nights],positions=[1,1.2]);
plt.ylim(0,10)
plt.grid()
```


    
![png](live_20210113_files/live_20210113_9_0.png)
    



```python
X = df.drop('available_more_20',axis=1).set_index('listing_id')
y = df.available_more_20

X
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>price</th>
      <th>latitude</th>
      <th>longitude</th>
      <th>minimum_nights</th>
      <th>a'dam</th>
      <th>amazing</th>
      <th>amstel</th>
      <th>amsterdam!</th>
      <th>amsterdam,</th>
      <th>apartment!</th>
      <th>...</th>
      <th>unique</th>
      <th>very</th>
      <th>vibrant</th>
      <th>view</th>
      <th>vondelpark</th>
      <th>west</th>
      <th>westerpark</th>
      <th>room_type_Entire home/apt</th>
      <th>room_type_Private room</th>
      <th>room_type_Shared room</th>
    </tr>
    <tr>
      <th>listing_id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2818</th>
      <td>63.051613</td>
      <td>52.365755</td>
      <td>4.941419</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3209</th>
      <td>161.326165</td>
      <td>52.390225</td>
      <td>4.873924</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>20168</th>
      <td>112.847201</td>
      <td>52.365087</td>
      <td>4.893541</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>25428</th>
      <td>125.000000</td>
      <td>52.373114</td>
      <td>4.883668</td>
      <td>14</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>27886</th>
      <td>148.477547</td>
      <td>52.386727</td>
      <td>4.892078</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>30576148</th>
      <td>349.000000</td>
      <td>52.345999</td>
      <td>4.952145</td>
      <td>7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>30577727</th>
      <td>189.000000</td>
      <td>52.362412</td>
      <td>4.932467</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>30578037</th>
      <td>80.000000</td>
      <td>52.362431</td>
      <td>4.926912</td>
      <td>10</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>30579673</th>
      <td>65.000000</td>
      <td>52.363780</td>
      <td>4.932493</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>30580413</th>
      <td>220.000000</td>
      <td>52.346911</td>
      <td>4.901932</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>10676 rows × 104 columns</p>
</div>




```python
from sklearn.model_selection import train_test_split

X_tr, X_ts, y_tr, y_ts = train_test_split(X,y,train_size=.2)
```


```python
X_ts
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>price</th>
      <th>latitude</th>
      <th>longitude</th>
      <th>minimum_nights</th>
      <th>a'dam</th>
      <th>amazing</th>
      <th>amstel</th>
      <th>amsterdam!</th>
      <th>amsterdam,</th>
      <th>apartment!</th>
      <th>...</th>
      <th>unique</th>
      <th>very</th>
      <th>vibrant</th>
      <th>view</th>
      <th>vondelpark</th>
      <th>west</th>
      <th>westerpark</th>
      <th>room_type_Entire home/apt</th>
      <th>room_type_Private room</th>
      <th>room_type_Shared room</th>
    </tr>
    <tr>
      <th>listing_id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>8502593</th>
      <td>150.000000</td>
      <td>52.343798</td>
      <td>4.911370</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>7947977</th>
      <td>139.971429</td>
      <td>52.349948</td>
      <td>4.890349</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>22662888</th>
      <td>302.419355</td>
      <td>52.373567</td>
      <td>4.902563</td>
      <td>4</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>19729119</th>
      <td>55.545714</td>
      <td>52.347738</td>
      <td>4.910687</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16539738</th>
      <td>305.168823</td>
      <td>52.393701</td>
      <td>4.910761</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>24930287</th>
      <td>80.899877</td>
      <td>52.351445</td>
      <td>4.850150</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3317675</th>
      <td>94.000000</td>
      <td>52.376722</td>
      <td>4.934967</td>
      <td>60</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>21458998</th>
      <td>199.380000</td>
      <td>52.376409</td>
      <td>4.887482</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5741450</th>
      <td>140.497696</td>
      <td>52.377246</td>
      <td>4.884149</td>
      <td>3</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15668884</th>
      <td>79.000000</td>
      <td>52.364006</td>
      <td>4.926288</td>
      <td>2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>8541 rows × 104 columns</p>
</div>




```python
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

param = {
    'max_depth': [3,6,9,12],
}

rf = RandomForestClassifier(n_estimators=500, random_state=61658, n_jobs=2)
grid = GridSearchCV(rf, param, cv=5, scoring='roc_auc', verbose=5)
```


```python
grid.fit(X_tr,y_tr)
```

    Fitting 5 folds for each of 4 candidates, totalling 20 fits
    [CV 1/5] END ....................................max_depth=3; total time=  10.7s
    [CV 2/5] END ....................................max_depth=3; total time=   4.6s
    [CV 3/5] END ....................................max_depth=3; total time=   4.6s
    [CV 4/5] END ....................................max_depth=3; total time=   4.0s
    [CV 5/5] END ....................................max_depth=3; total time=   4.4s
    [CV 1/5] END ....................................max_depth=6; total time=   5.1s
    [CV 2/5] END ....................................max_depth=6; total time=   5.8s
    [CV 3/5] END ....................................max_depth=6; total time=   6.0s
    [CV 4/5] END ....................................max_depth=6; total time=   7.6s
    [CV 5/5] END ....................................max_depth=6; total time=   6.1s
    [CV 1/5] END ....................................max_depth=9; total time=   4.6s
    [CV 2/5] END ....................................max_depth=9; total time=   6.1s
    [CV 3/5] END ....................................max_depth=9; total time=   7.3s
    [CV 4/5] END ....................................max_depth=9; total time=   8.7s
    [CV 5/5] END ....................................max_depth=9; total time=   6.6s
    [CV 1/5] END ...................................max_depth=12; total time=   5.2s
    [CV 2/5] END ...................................max_depth=12; total time=   5.2s
    [CV 3/5] END ...................................max_depth=12; total time=   4.5s
    [CV 4/5] END ...................................max_depth=12; total time=   6.1s
    [CV 5/5] END ...................................max_depth=12; total time=   6.4s





    GridSearchCV(cv=5,
                 estimator=RandomForestClassifier(n_estimators=500, n_jobs=2,
                                                  random_state=61658),
                 param_grid={'max_depth': [3, 6, 9, 12]}, scoring='roc_auc',
                 verbose=5)




```python
grid.best_params_
```




    {'max_depth': 9}




```python
grid.best_score_
```




    0.6679508757032396




```python
from sklearn.metrics import roc_auc_score

roc_auc_score(y_ts, grid.predict_proba(X_ts)[:,1])
```




    0.6727976480975534




```python
grid.predict(X_ts)
```




    array([0, 0, 0, ..., 0, 1, 0])




```python
grid.predict_proba(X_ts)[:,1]
```




    array([0.30014489, 0.31274363, 0.34707202, ..., 0.33683495, 0.54017508,
           0.24967793])




```python
grid.best_estimator_.feature_importances_
```




    array([0.16569002, 0.08371877, 0.09467396, 0.10087638, 0.00151923,
           0.00323216, 0.00231042, 0.00113113, 0.00367509, 0.00195385,
           0.0025954 , 0.00277255, 0.00476003, 0.00254846, 0.00429522,
           0.00092438, 0.00321048, 0.0028491 , 0.00857595, 0.00593351,
           0.00131351, 0.00151629, 0.0069263 , 0.00708678, 0.00649905,
           0.00534246, 0.00757509, 0.00180457, 0.00320118, 0.00730106,
           0.00815169, 0.00375482, 0.00205768, 0.01098696, 0.00486014,
           0.00357229, 0.00206673, 0.00210406, 0.00428067, 0.0192531 ,
           0.00316113, 0.00578879, 0.00426903, 0.00615686, 0.00639461,
           0.00442677, 0.00278638, 0.00410342, 0.01132655, 0.00865771,
           0.00423521, 0.00528335, 0.00345009, 0.00958338, 0.00549824,
           0.00391571, 0.0019691 , 0.00351097, 0.00748508, 0.0033124 ,
           0.00520963, 0.00571428, 0.00451143, 0.00783265, 0.00191971,
           0.00037907, 0.00255247, 0.00382175, 0.00132962, 0.00727341,
           0.00366397, 0.00184062, 0.00453967, 0.00439625, 0.00204426,
           0.01035115, 0.00492965, 0.00245214, 0.00179752, 0.00258035,
           0.00867044, 0.00275812, 0.01491995, 0.00161803, 0.00289888,
           0.00672569, 0.00301403, 0.00207546, 0.00725467, 0.00209613,
           0.00261685, 0.00382511, 0.00977065, 0.00484075, 0.00435958,
           0.0021417 , 0.00218405, 0.00661003, 0.00427901, 0.00739524,
           0.00177572, 0.05307749, 0.05255063, 0.00118688])




```python
X_tr.columns
```




    Index(['price', 'latitude', 'longitude', 'minimum_nights', 'a'dam', 'amazing',
           'amstel', 'amsterdam!', 'amsterdam,', 'apartment!',
           ...
           'unique', 'very', 'vibrant', 'view', 'vondelpark', 'west', 'westerpark',
           'room_type_Entire home/apt', 'room_type_Private room',
           'room_type_Shared room'],
          dtype='object', length=104)




```python

```


```python
imps = grid.best_estimator_.feature_importances_
order = np.argsort(imps)[::-1]
cols = X_tr.columns

for imp,col in zip(imps[order],cols[order]):
    print( f'{col:30s} {imp:.3f}')
```

    price                          0.166
    minimum_nights                 0.101
    longitude                      0.095
    latitude                       0.084
    room_type_Entire home/apt      0.053
    room_type_Private room         0.053
    family                         0.019
    room                           0.015
    home                           0.011
    cosy                           0.011
    private                        0.010
    terrace                        0.010
    light                          0.010
    roof                           0.009
    house                          0.009
    beautiful                      0.009
    close                          0.008
    near                           0.008
    centre                         0.008
    lovely                         0.007
    west                           0.007
    clean                          0.007
    park                           0.007
    studio                         0.007
    canal                          0.007
    bright                         0.007
    square                         0.007
    view                           0.007
    center                         0.006
    garden                         0.006
    from                           0.006
    bedroom                        0.006
    floor                          0.006
    modern                         0.006
    located                        0.005
    central                        0.005
    jordaan                        0.005
    luxury                         0.005
    quiet                          0.005
    cozy                           0.005
    trendy                         0.005
    appartment                     0.005
    perfect                        0.005
    museum                         0.005
    great                          0.004
    pijp                           0.004
    unique                         0.004
    area                           0.004
    east                           0.004
    vondelpark                     0.004
    free                           0.004
    houseboat                      0.004
    heart                          0.004
    location                       0.004
    sunny                          0.004
    nice                           0.004
    comfortable                    0.004
    amsterdam,                     0.004
    parking                        0.004
    design                         0.004
    loft                           0.004
    large                          0.003
    luxurious                      0.003
    amazing                        0.003
    balcony                        0.003
    charming                       0.003
    flat                           0.003
    station                        0.003
    spacious,                      0.003
    bathroom                       0.003
    ground                         0.003
    appartement                    0.003
    rooftop                        0.003
    stylish                        0.003
    apartment,                     0.003
    romantic                       0.003
    next                           0.003
    apt.                           0.003
    renovated                      0.002
    amstel                         0.002
    vibrant                        0.002
    very                           0.002
    double                         0.002
    stunning                       0.002
    stay                           0.002
    district                       0.002
    comfy                          0.002
    place                          0.002
    location!                      0.002
    apartment!                     0.002
    nearby                         0.002
    penthouse                      0.002
    centre!                        0.002
    river                          0.002
    westerpark                     0.002
    south                          0.002
    a'dam                          0.002
    bikes                          0.002
    north                          0.001
    best                           0.001
    room_type_Shared room          0.001
    amsterdam!                     0.001
    authentic                      0.001
    neighbourhood                  0.000



```python
plt.bar(range(imps.shape[0]),imps[order])
plt.xlim(-1,50)
```




    (-1.0, 50.0)




    
![png](live_20210113_files/live_20210113_24_1.png)
    



```python

```
